import pygame

def write_text(size, text, cords, screen, color=(0, 0, 0)):
    screen.blit(
        pygame.font.SysFont("None", size).render(text, 0, color), cords)

def x_scale(step, cords_x_line, stop_draw_x_line, x_display, FontSise, y_display, screen): #x_display, FontSise, y_display, screen
    x_chkala = 0
    a_y = cords_x_line[1][1]
    for a_x in range(cords_x_line[0][0], cords_x_line[1][0], step):
        if x_chkala != 0 and a_x <= stop_draw_x_line - 50:
            if x_chkala / step % 10 == 0:
                pygame.draw.line(screen, (0, 30, 0), [a_x + x_display // 30, a_y - FontSise // 10],
                                 [a_x + x_display // 30, a_y + FontSise // 6], 3)
                write_text(int(FontSise * 0.6), str(x_chkala),
                           (a_x + x_display // 48, a_y + y_display // 100), screen)
            else:
                pygame.draw.line(screen, (0, 0, 0), [a_x + x_display // 30, a_y - FontSise // 10],
                                 [a_x + x_display // 30, a_y + FontSise // 10], 3)
        x_chkala += step


def y_scale(step, cords_x_line, cords_y_line, stop_draw_y_line, screen, y_display, x_display, FontSise): #screen, y_display, x_display, FontSise
    y_chkala = 0
    const_cord_x = cords_y_line[0][0]
    for cord_y in range(cords_x_line[0][1], cords_y_line[0][1], -step):
        if y_chkala != 0 and cord_y >= stop_draw_y_line:
            if y_chkala / step % 10 == 0:
                pygame.draw.line(screen, (0, 30, 0), [const_cord_x - x_display // 200, cord_y],
                                 [const_cord_x + x_display // 550, cord_y], 3)
                write_text(int(FontSise * 0.6), str(y_chkala),
                           (const_cord_x - x_display // 40, cord_y - y_display // 200), screen)
            else:
                pygame.draw.line(screen, (0, 0, 0), [const_cord_x - x_display // 400, cord_y],
                                 [const_cord_x + x_display // 550, cord_y], 3)
        y_chkala += step

def draw_scale(cords_x_line, cords_y_line, x_display, gap, screen, y_display, FontSise): #x_display, gap, screen, y_display, FontSise
    step = 10
    x_scale(step, cords_x_line, x_display - gap, x_display, FontSise, y_display, screen)
    y_scale(step, cords_x_line, cords_y_line, 30, screen, y_display, x_display, FontSise)


def draw_arrow(first_line_cords, second_line_cords, text, mode, FontSise, screen): #FontSise, screen
    screen.blit(pygame.font.SysFont("None", FontSise-10).render(text, 0, (0, 0, 0)),
                (mode[0], mode[1]))
    pygame.draw.line(screen, (0, 0, 0), first_line_cords[0], first_line_cords[1],
                     FontSise // 10)
    pygame.draw.line(screen, (0, 0, 0), second_line_cords[0], second_line_cords[1],
                     FontSise // 10)


def draw_point_zero(screen, gap, FontSise, y_display): #screen, gap, FontSise, y_display
    screen.blit(pygame.font.SysFont("None", FontSise).render(str(0), 0, (0, 0, 0)),
                (gap - FontSise // 2, y_display - gap + 20))
    pygame.draw.circle(screen, (0, 0, 0), (gap, y_display - gap), 5)


def draw_axis(mode, screen, FontSise): #screen, FontSise
    pygame.draw.line(screen, (0, 200, 60), mode[0], mode[1],
                     FontSise // 6)


def cord(screen, color_display, y_display, gap, x_display, FontSise):
    screen.fill(color_display)
    draw_axis([[5, y_display - gap], [x_display - 5, y_display - gap]], screen, FontSise)  # Ox
    draw_axis([[gap, 5], [gap, y_display + gap]], screen, FontSise)  # Oy

    Ox = [[5, y_display - gap], [x_display - 5, y_display - gap]]
    Oy = [[gap, 5], [gap, y_display + gap]]

    draw_point_zero(screen, gap, FontSise, y_display)

    draw_arrow([[gap, x_display // 900], [gap - y_display // 90, x_display // 72]],
               [[gap, x_display // 900], [gap + y_display // 90, x_display // 72]], 'h, м',
               (FontSise // 4.5, FontSise // 1.5), FontSise, screen)

    draw_arrow([[x_display - x_display // 50, y_display - gap],
                [x_display - x_display // 30, y_display - gap + y_display // 90]],
               [[x_display - x_display // 50, y_display - gap],
                [x_display - x_display // 30, y_display - gap - y_display // 90]], 'S, м',
               (x_display - int(FontSise * 2.5), y_display - FontSise // 2), FontSise, screen)

    write_text(int(FontSise/1.5), 'Клафиши управления:', (2 * gap, y_display*1.12), screen, (0, 0, 0))
    write_text(int(FontSise/1.5),
               'регулеровка скорости - "влево" и "вправо", регулировка угла - "вверх" и "вниз", начертить график - "Enter"',
               (2 * gap, y_display*1.15), screen, (0, 0, 0))
    draw_scale(Ox, Oy, x_display, gap, screen, y_display, FontSise)
