import pygame
from pygame.locals import *
from math import sin, cos
from random import randint
from physics_pygame_project_1_function_p import write_text, draw_scale, draw_point_zero, draw_axis, cord

try:
    def draw_gun(x, y, angle):
        player_image = pygame.transform.rotate(image, angle)
        player_rect = player_image.get_rect()
        player_rect.center = (x, y)
        screen.blit(player_image, player_rect)
        screen.blit(wheel, (x - 20, y - 20))


    image = pygame.image.load('gun32.png')
    wheel = pygame.image.load('wheel1.png')


    def get_a_v(x, y, color_display):
        global circles_list
        xn, yn = x, y
        x_text_pos, y_text_pos = (xn + 70)*1.05, (yn + 55)*1.05

        def rot_center(image, angle):
            pygame.draw.circle(screen, color_display, (xn, yn), int(x_display*0.06))
            creating_a_coordinate_system()

            circles_group.draw(screen)
            for i in circles_list:
                pygame.draw.circle(i[0], i[1], i[2], i[3])
            player_image = pygame.transform.rotate(image, angle)
            player_rect = player_image.get_rect()
            player_rect.center = (xn, yn)
            screen.blit(player_image, player_rect)
            screen.blit(wheel, (xn - 20, yn - 20))

        angle = 0
        rot_center(image, angle)
        clock = pygame.time.Clock()
        x_v = 0
        y += 20

        color_li = [0, 255, 0]
        n_li = [1, 0]
        k, n = 5, 0

        button_cords = (x_display * 0.85, y_display * 1.017, x_display * 0.11, y_display * 0.077)
        drow_rect((0, 0, 100), (x_display * 0.846, y_display * 1.01, x_display * 0.119, y_display * 0.093), screen)
        drow_rect((255, 239, 213), button_cords, screen)
        write_text(int(FontSise / 1.5), 'Перезапустить', (button_cords[0] + 5, button_cords[1] + 10), screen, (0, 0, 0))

        rect_size = (x_text_pos*1.1 + x_v, y * 1.01, x_text_pos * 0.03, x_text_pos * 0.16)
        pygame.draw.rect(screen, color_display, (
            (x + 70) * 1.05 * 1.09, (y * 1.01) * 1.05, (x + 70) * 1.05 * 1.3, (y * 0.08)))
        pygame.draw.rect(screen, (0, 255, 0), rect_size)
        screen.blit(pygame.font.SysFont("None", int(FontSise / 1.4)).render('v, м/с = ' + str(x_v // 2), 0,
                                                                            (0, 0, 0)),
                    ((x + 70) * 1.05 * 1.1 + x_v, (y * 1.01) * 1.07))
        screen.blit(
            pygame.font.SysFont("None", int(FontSise / 1.4)).render('a, ° = ' + str(90 - abs(angle)), 0,
                                                                    (0, 0, 0)),
            ((x + 70) * 1.05 * 1.1 + x_v, (y * 1.01) * 1.1))
        pygame.display.flip()  # !

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    raise SystemExit("Вы хотите закрыть окно?")
                if event.type == pygame.KEYDOWN:
                    rect_size = (x_text_pos*1.1 + x_v, y*1.01, x_text_pos*0.03, x_text_pos*0.16)
                    color = (color_li[0], color_li[1], color_li[2])
                    if event.key == pygame.K_RIGHT:
                        if color[0] + k <= 255 and color[1] + k >= 0:
                            x_v += 3
                            color_li[n] += k
                            pygame.draw.rect(screen, color, rect_size)
                    elif event.key == pygame.K_LEFT:
                        if color[1] - k <= 255 and color[0] - k >= 0:
                            x_v -= 3
                            color_li[n] -= k
                            pygame.draw.rect(screen, color_display, rect_size)
                    elif event.key == pygame.K_UP and angle <= 0:
                        angle += 1
                        rot_center(image, angle)
                    elif event.key == pygame.K_DOWN and -90 <= angle:
                        angle -= 1
                        rot_center(image, angle)
                    elif event.key == pygame.K_RETURN:
                        pygame.draw.rect(screen, color_display, (x_text_pos*1.1, y * 1.01, x_text_pos * 0.03 + x_v, x_text_pos * 0.16))
                        pygame.draw.rect(screen, color_display, (
                            (x + 70) * 1.05 * 1.09, (y * 1.01) * 1.05, (x + 70) * 1.05 * 1.3, (y * 0.08)))
                        return x_v // 2, 90 - abs(angle)

                    if color_li[0] == 255 and color_li[1] == 255:
                        n = n_li[n]
                        k = -k
                    pygame.draw.rect(screen, color_display, (
                        (x + 70) * 1.05 * 1.09, (y * 1.01) * 1.05, (x + 70)*1.05*1.3, (y*0.08)))
                    screen.blit(pygame.font.SysFont("None", int(FontSise / 1.4)).render('v, м/с = ' + str(x_v // 2), 0,
                                                                                        (0, 0, 0)),
                                ((x + 70)*1.05*1.1, (y*1.01)*1.07))
                    screen.blit(
                        pygame.font.SysFont("None", int(FontSise / 1.4)).render('a, ° = ' + str(90 - abs(angle)), 0,
                                                                                (0, 0, 0)),
                        ((x + 70)*1.05*1.1, (y*1.01)*1.1))
                    pygame.display.flip()  # !
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    rot_center(image, 0)
                    continue_program(event.pos, button_cords)
            clock.tick(30)


    ##################################деления на шкалах########################################
    def drow_rect(color, cords, scre):
        pygame.draw.rect(scre, color, cords)


    def continue_program(pos, cords):
        global circles_list, text_list
        if cords[0] < pos[0] < cords[0] + cords[2] and cords[1] < pos[1] < cords[1] + cords[3]:
            cord(screen, color_display, y_display, gap, x_display, FontSise)
            circles_list = []
            text_list = []
            make_the_circle_fly(x_display, y_display, color_display)


    def start_screen(x1, y1, color_display):
        global x, y
        fon = pygame.transform.scale(pygame.image.load('physics_fon_3.jpg'), (x1, y1))
        surfase.blit(fon, (0, 0))
        drow_rect((0, 0, 100), (20, 235, 1160, 80), surfase)
        drow_rect((255, 255, 235), (25, 240, 1150, 70), surfase)
        write_text(int(y * 0.068), 'Движение полёта тела, брошенного под углом к горизонту', (46, 255), surfase, (0, 0, 100))
        drow_rect((0, 0, 100), (7, 7, 406, 86), surfase)
        drow_rect(color_display, (10, 10, 400, 80), surfase)
        surfase.blit(
            pygame.font.SysFont("None", int(y1 * 0.03)).render("отрегулируйте размер экрана", 0, (0, 0, 0)),
            (int(y1 * 0.06), int(y1 * 0.03)))
        surfase.blit(
            pygame.font.SysFont("None", int(y1 * 0.03)).render("клавишими 'вверх'/'вниз'", 0, (0, 0, 0)),
            (int(y1 * 0.06), int(y1 * 0.05)))
        surfase.blit(
            pygame.font.SysFont("None", int(y1 * 0.03)).render(str(float(x / x1))[:3], 0, (0, 0, 0)),
            (int(y1 * 0.06), int(y1 * 0.075)))

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    mainLoopik = False
                    raise SystemExit("Вы хотите закрыть окно?")
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP and float(x / x1) < 1.3:
                        x += 20
                        y += 20
                    elif event.key == pygame.K_DOWN and float(x / x1) > 0.7:
                        x -= 20
                        y -= 20
                    elif event.key == pygame.K_RETURN:
                        return x, y
                    if 0.7 < float(x / x1) < 1.3:
                        drow_rect(color_display, (10, int(y1 * 0.07), 400, 30), surfase)
                        surfase.blit(
                            pygame.font.SysFont("None", int(y1 * 0.03)).render(str(float(x / x1))[:3], 0, (0, 0, 0)),
                            (int(y1 * 0.06), int(y1 * 0.075)))
                        pygame.display.flip()
            pygame.display.flip()  # !


    class Circles(pygame.sprite.Sprite):
        def __init__(self, group, x, y, color, r):
            super().__init__(group)
            self.radius = r
            self.image = pygame.Surface((2 * r, 2 * r),
                                        pygame.SRCALPHA, 32)
            pygame.draw.circle(screen, color, (x, y), r)
            self.rect = self.image.get_rect()
            self.rect.x = x
            self.rect.y = y


    circles_group = pygame.sprite.Group()
    circles_list = []


    def make_the_circle_fly(x_display, y_display, color_display):
        global text_list
        clock = pygame.time.Clock()
        COLOR = (0, 0, 100)
        sht_x = x_display // 3
        sht_y = 30
        a_color = COLOR
        c = 10


        for little_c in range(c):
            x_cir, y_cir = 0, y_display
            '''V'''
            '''a'''
            v, degrees_a = get_a_v(gap, y_display - gap, color_display)

            a = (3.14 / 180) * degrees_a
            t = int(v * sin(a) * 2 / 9.8)
            h = int(v ** 2 * (sin(a)) ** 2 / (2 * 9.8))
            s = int(v ** 2 * sin(2 * a) / 9.8)
            sht_y += 40
            write_text(FontSise, 's = {}м,  h(маx) = {}м,  t = {}c,  v = {}м/c,  α = {}°'.format(s, h, t, v, degrees_a),
                       (sht_x, sht_y - int(sht_y * 0.2)), screen, COLOR)
            text_list.append((s, h, t, v, degrees_a, sht_x, sht_y - int(sht_y * 0.2), COLOR))
            indik = True
            for i in range(0, t + 1):
                for j in range(100):
                    i += 0.01
                    for j in pygame.event.get():
                        if j.type == QUIT:
                            raise SystemExit("Вы хотите закрыть окно?")

                    pygame.draw.circle(screen, COLOR, (x_cir + gap, int(y_cir) - gap), int(FontSise / 15))  # =r
                    pygame.display.flip()  # !

                    if x_cir + gap <= x_display // 12:
                        circles_list.append((screen, COLOR, (x_cir + gap, int(y_cir) - gap), int(FontSise / 15)))
                        draw_gun(gap, y_display - gap, degrees_a - 90)

                    if y_cir <= y_display and (x_cir <= x_display or s <= x_display):
                        y_cir = y_display - ((v * sin(a) * i) - (9.8 * i ** 2 / 2))
                        x_cir = int(v * cos(a) * i)
                    else:#отрисовка инфы после графиков
                        for i in text_list:
                            s, h, t, v, a, x, y, color_text = i
                            drow_rect(color_display, (x*0.98, y*0.98, x_display * 0.55, y_display * 0.05),
                                      screen)
                            write_text(FontSise, 's = {}м,  h(маx) = {}м,  t = {}c,  v = {}м/c,  α = {}°'.format(s,h,t,v,a),
                                       (x, y), screen, color_text)
                        while COLOR == a_color or (COLOR[0] > 200 and COLOR[1] > 200 and COLOR[2] > 200):
                            a_color = (randint(0, 255), randint(0, 255), randint(0, 255))
                        COLOR = a_color
                        indik = False
                        break

                if indik == False:
                    break

                clock.tick(20)

        mainLoopik = True
        write_text(FontSise, 'Больше графики чертить нельзя', (int(x_display // 50), 20), (255, 0, 0))
        while mainLoopik:
            for j in pygame.event.get():
                if j.type == QUIT:
                    mainLoopik = False
                    raise SystemExit("Вы хотите закрыть окно?")


    def creating_a_coordinate_system():
        draw_axis([[5, y_display - gap], [x_display // 8, y_display - gap]], screen, FontSise)  # Ox
        draw_axis([[gap, y_display - 3 * gap], [gap, y_display + gap]], screen, FontSise)  # Oy
        Ox = [[5, y_display - gap], [x_display // 10, y_display - gap]]
        Oy = [[gap, y_display - 3 * gap], [gap, y_display + gap]]
        draw_point_zero(screen, gap, FontSise, y_display)
        draw_scale(Ox, Oy, x_display, gap, screen, y_display, FontSise)


    ############################################программа####################################
    pygame.init()
    pygame.key.set_repeat(1, 70)

    mainLoop = True  # флаг окна
    while mainLoop:
        for event in pygame.event.get():
            if event.type == QUIT:
                mainLoop = False

        pygame.init()
        color_display = (255, 255, 242)
        x, y = 1200, 800
        clock = pygame.time.Clock()
        surfase = pygame.display.set_mode((x, y), 0, 32)
        x, y = start_screen(x, y, color_display)
        text_list = []

        (x_display, y_display) = (x, y)
        screen = pygame.display.set_mode((x_display, y_display), 0, 32)
        pygame.display.set_caption("Привет, Физика!")
        FontSise = int(45 / 1500 * x_display)
        gap = int((60 / 1500) * y_display) + y_display // 50
        y_display -= y_display // 6
        x_display += x_display // 50

        # создание системы коардинат
        cord(screen, color_display, y_display, gap, x_display, FontSise)
        circles_list = []
        make_the_circle_fly(x_display, y_display, color_display)




except ValueError as e:
    mainLoop = True
    while mainLoop:
        for event in pygame.event.get():
            if event.type == QUIT:
                mainLoop = False
        screen.blit(
            pygame.font.SysFont("None", FontSise).render('Ошибка ввода, перезапустите программу', 0, (200, 0, 0)),
            (x_display // 4, y_display // 8))
        pygame.display.update()

pygame.quit()
